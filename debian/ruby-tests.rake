#-*- mode: ruby; coding: utf-8 -*-
require 'rake/testtask'
task :default => :test
Rake::TestTask.new do |t|
  t.libs << 'lib' << 'test'
  t.test_files = FileList['test/test_*.rb'].exclude(/.*tag.*|.*plugin.*|.*convertible.*|.*theme.*/) - FileList['test/test_site.rb','test/test_new_command.rb']
  t.verbose = true
end
